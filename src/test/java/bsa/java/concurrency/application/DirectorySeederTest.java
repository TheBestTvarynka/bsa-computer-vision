package bsa.java.concurrency.application;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import bsa.java.concurrency.conf.DirectorySeeder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

@WebMvcTest(DirectorySeeder.class)
public class DirectorySeederTest {

    @Value("${root_image_dir}")
    private String imageDir;

    @Autowired
    private DirectorySeeder directorySeeder;

    @Test
    public void whenSeed_thenCreateDirectories() {
        directorySeeder.seed(null);

        assertTrue(Files.exists(Paths.get(imageDir)));

        try {
            Files.walk(Paths.get(imageDir))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            fail(e);
        }
    }

}
