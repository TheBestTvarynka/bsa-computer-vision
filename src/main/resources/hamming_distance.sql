
create or replace function hamming_distance(hash1 text, hash2 text) returns integer as $$
    declare
        distance integer = 0;
    begin
        for index in 1..64 loop
            if substr(hash1, index, 1) != substr(hash2, index, 1) then
                distance = distance + 1;
            end if;
        end loop;
        return distance;
    end;
    $$ language plpgsql^;

