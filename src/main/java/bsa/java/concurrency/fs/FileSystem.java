package bsa.java.concurrency.fs;

import bsa.java.concurrency.conf.ThreadManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.concurrent.CompletableFuture;

@Repository
public class FileSystem {
    private static Logger logger = LogManager.getLogger(FileSystem.class);

    public CompletableFuture<String> saveFileAsync(String path, byte[] file) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                FileOutputStream fileStream = new FileOutputStream(new File(path));
                fileStream.write(file);
                fileStream.close();
                logger.info("All data has been saved to file: " + path);
            } catch (IOException e) {
                logger.error("Unable to save data in file " + path + " . Details:");
                logger.error(e.toString());
            }
            return path;
        }, ThreadManager.getExecutorService());
    }

    public CompletableFuture<Void> deleteFileAsync(String path) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                Files.deleteIfExists(Paths.get(path));
                logger.info("File deleted: " + path);
            } catch (IOException e) {
                logger.error("Unable to delete file " + path + " . Details:");
                logger.error(e.toString());
            }
            return null;
        });
    }

    public CompletableFuture<Void> deleteAllFilesInDirAsync(String pathToDir) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                Files.walk(Paths.get(pathToDir))
                        .sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        .filter(File::isFile)
                        .forEach(File::delete);
                logger.info("Directory " + pathToDir + " cleared.");
            } catch (IOException e) {
                logger.error("Unable to delete files in directory: " + pathToDir + " . Details:");
                logger.error(e.toString());
            }
            return null;
        });
    }

}
