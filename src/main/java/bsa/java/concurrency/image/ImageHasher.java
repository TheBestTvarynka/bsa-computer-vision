package bsa.java.concurrency.image;

import bsa.java.concurrency.conf.ThreadManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class ImageHasher {
    private static Logger logger = LogManager.getLogger(ImageHasher.class);

    private static BufferedImage convertToBufferedImage(Image image) {
        BufferedImage newImage = new BufferedImage(
                image.getWidth(null), image.getHeight(null),
                BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = newImage.createGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return newImage;
    }

    private static float getBrightness(int pixel) {
//        int alpha = (pixel >> 24) & 0xff;
        int red = (pixel >> 16) & 0xff;
        int green = (pixel >> 8) & 0xff;
        int blue = pixel & 0xff;
        return (red * 0.2126f + green * 0.7152f + blue * 0.0722f) / 255;
    }

    public static BufferedImage greyScaleFilter(BufferedImage image) {
        //get image width and height
        int width = image.getWidth();
        int height = image.getHeight();

        //convert to grayscale
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int p = image.getRGB(x, y);

                int a = (p >> 24) & 0xff;
                int r = (p >> 16) & 0xff;
                int g = (p >> 8) & 0xff;
                int b = p & 0xff;

                //calculate average
                int avg = (r + g + b) / 3;

                //replace RGB value with avg
                p = (a << 24) | (avg << 16) | (avg << 8) | avg;

                image.setRGB(x, y, p);
            }
        }
        return image;
    }

    public static long dHash(BufferedImage image) {
        long hash = 0L;

        //get image width and height
        int width = image.getWidth();
        int height = image.getHeight();
        if (width != 9 || height != 9) {
            return -1L;
        }

        float brightness1, brightness2;

        for (int y = 1; y < height; y++) {
            for (int x = 1; x < width; x++) {
                brightness1 = getBrightness(image.getRGB(x, y));
                brightness2 = getBrightness(image.getRGB(x - 1, y - 1));
                if (brightness1 > brightness2) {
                    hash |= 1;
                }
                hash = hash << 1;
            }
        }
        return hash;
    }

    public static long getHashOf(Image image) {
        Image smallImage = image.getScaledInstance(9, 9, Image.SCALE_DEFAULT);
        BufferedImage bufferedSmallGreyImage = greyScaleFilter(convertToBufferedImage(smallImage));
        return dHash(bufferedSmallGreyImage);
    }

    public static CompletableFuture<Long> getHashOfAsync(Image image) {
        return CompletableFuture.supplyAsync(() -> {
            logger.info("start hashing");
            Image smallImage = image.getScaledInstance(9, 9, Image.SCALE_DEFAULT);
            BufferedImage bufferedSmallGreyImage = greyScaleFilter(convertToBufferedImage(smallImage));
            long hash = dHash(bufferedSmallGreyImage);
            logger.info("end hashing");
            return hash;
        }, ThreadManager.getExecutorService());
    }

    public static Image byteArrayToImage(byte[] data) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
        try {
            return ImageIO.read(inputStream);
        } catch (IOException e) {
            logger.error("Unable to convert byte[] to Image. Details:");
            logger.error(e);
            return null;
        }
    }

}
