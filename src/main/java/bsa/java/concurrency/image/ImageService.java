package bsa.java.concurrency.image;

import bsa.java.concurrency.conf.ThreadManager;
import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.model.Image;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ImageService {
    private static Logger logger = LogManager.getLogger(ImageService.class);

    @Value("${image_dir}")
    String imageDir;

    @Autowired
    FileSystem fileSystem;

    @Autowired
    ImageRepository imageRepository;

    public void saveFiles(MultipartFile[] files) {
        List<Pair<String, byte[]>> filesList = Stream.of(files)
                .map(file -> {
                    try {
                        return Pair.of(file.getOriginalFilename(), file.getBytes());
                    } catch (IOException e) {
                        logger.error("Unable to extract bytes from passed image. Details:");
                        logger.error(e);
                        return null;
                    }
                })
                .collect(Collectors.toList());
        List<CompletableFuture<Object>> jobs = filesList.stream()
                .map(file -> fileSystem.saveFileAsync(imageDir + file.getFirst(), file.getSecond())
                        .thenCombineAsync(
                                ImageHasher.getHashOfAsync(ImageHasher.byteArrayToImage(file.getSecond())),
                                (path, hash) -> {
                                    saveImageMetadata(hash, path);
                                    return null;
                                }, ThreadManager.getExecutorService()))
                .collect(Collectors.toList());
        CompletableFuture[] futures = new CompletableFuture[jobs.size()];
        jobs.toArray(futures);
        try {
            CompletableFuture.allOf(futures).get();
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Error. Details:");
            logger.error(e);
        }
        logger.info("Finished saving images!");
    }

    public void saveImageMetadata(Long hash, String path) {
        String formattedHash = Image.formatHash(hash);
        logger.info("Save new Image in database with hash=" + formattedHash + " and path=" + path);
        Image image = new Image();
        image.setDHash(formattedHash);
        image.setPath(path);
        imageRepository.save(image);
    }

    public List<SearchResultDTO> searchMatches(MultipartFile file, double threshold) {
        String fileName = file.getOriginalFilename();
        byte[] imageData = null;
        try {
            imageData = file.getBytes();
        } catch (IOException e) {
            logger.error("Unable to extract bytes from passed image. Details:");
            logger.error(e);
        }
        long hash = ImageHasher.getHashOf(ImageHasher.byteArrayToImage(imageData));

        List<SearchResultDTO> result = imageRepository.findAllMatchers(Image.formatHash(hash), threshold);

        if (result.size() == 0) {
            logger.info("Matches not found! Saving passed image...");
            fileSystem.saveFileAsync(imageDir + fileName, imageData)
                    .thenApply(path -> {
                        saveImageMetadata(hash, path);
                        return null;
                    });
        }
        return result;
    }

    public void deleteImageById(UUID id) {
        Optional<Image> imageToDelete = imageRepository.findById(id);
        if (imageToDelete.isEmpty()) {
            logger.info("Unable to delete the Image. Image with id=" + id + " does not exist. Nothing to do.");
            return;
        }
        Image image = imageToDelete.get();
        fileSystem.deleteFileAsync(image.getPath())
                .thenRun(() -> imageRepository.delete(image));
    }

    public void deleteAllImages() {
        fileSystem.deleteAllFilesInDirAsync(imageDir)
                .thenRun(() -> imageRepository.deleteAll());
    }

}
