package bsa.java.concurrency.image.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@AllArgsConstructor
public class SearchResultDTO {
    UUID id;
    Double matchPercent;
    String url;

    public SearchResultDTO(String id, BigDecimal matchPercent, String url) {
        this.id = UUID.fromString(id);
        this.matchPercent = matchPercent.doubleValue();
        this.url = url;
    }

}
