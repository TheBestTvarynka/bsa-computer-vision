package bsa.java.concurrency.image.model;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "images")
@SqlResultSetMapping(
        name = "findAllMatchersMapping",
        classes = {
                @ConstructorResult(
                        targetClass = SearchResultDTO.class,
                        columns = {
                                @ColumnResult(name = "id", type = String.class),
                                @ColumnResult(name = "percent", type = BigDecimal.class),
                                @ColumnResult(name = "url", type = String.class)
                        }
                )
        }
)
@NamedNativeQuery(name = "Image.findAllMatchers",
        query = "select cast(images.id as varchar) as id, hd.percent, images.path as url from images " +
                "left join (select id, 1.0 - hamming_distance(dhash, :hash) / 64.0 as percent from images) as hd on hd.id = images.id " +
                "where hd.percent >= :threshold",
        resultSetMapping = "findAllMatchersMapping")
public class Image {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "dhash",nullable = false)
    String dHash;

    @Column(nullable = false)
    String path;

    public static String formatHash(Long hash) {
        StringBuilder stringHash = new StringBuilder(Long.toBinaryString(hash));
        while (stringHash.length() < 64) {
            stringHash.insert(0, '0');
        }
        return stringHash.toString();
    }

    public static Long fromString(String hash) {
        return new BigInteger(hash, 2).longValue();
    }
}
