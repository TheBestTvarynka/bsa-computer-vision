package bsa.java.concurrency.conf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class DirectorySeeder {
    private static Logger logger = LogManager.getLogger(DirectorySeeder.class);

    @Value("${image_dir}")
    String imageDir;

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        try {
            Path imageDirectory = Paths.get(imageDir);
            if (!Files.exists(imageDirectory)) {
                logger.info("Creating directory for images...");
                Files.createDirectories(imageDirectory);
            }
        } catch (IOException e) {
            logger.error("Unable to seed directories. Details:");
            logger.error(e);
        }
    }
}
