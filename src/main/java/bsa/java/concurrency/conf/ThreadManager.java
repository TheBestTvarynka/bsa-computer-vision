package bsa.java.concurrency.conf;

import org.springframework.beans.factory.annotation.Value;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

public class ThreadManager {

    @Value("${thread_count}")
    private static int threadCount = 100;

    // limit number of threads
    private static ExecutorService executorService = null;

    public static ExecutorService getExecutorService() {
        if (executorService == null) {
            System.out.println(threadCount);
            executorService = new ForkJoinPool(threadCount);
        }
        return executorService;
    }

}
