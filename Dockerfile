FROM openjdk:11
VOLUME /tmp
EXPOSE 8080
ADD build/libs/concurrency-0.0.1-SNAPSHOT.jar cv.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/cv.jar"]
